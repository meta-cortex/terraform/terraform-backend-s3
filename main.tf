terraform {
    backend "s3" {
      bucket         = "terraform-state-bnet"
      key            = "terraform-backkend-s3/terraform.tfstate"
      region         = "us-east-1"
      dynamodb_table = "terraform_state"
    }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "us-east-1"

  default_tags {
    tags = {
      environment = "test"
      source      = "https://gitlab.com/meta-cortex/terraform/terraform-backend-s3"
      project     = "terraform backkend s3"
      terraform   = "true"
    }
  }
}

########## Dynamodb ##########
resource "aws_dynamodb_table" "terraform-lock" {
  name           = "terraform_state"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
  tags = {
    "Name" = "DynamoDB Terraform State Lock Table"
  }
}

########## S3 Bucket ##########
resource "aws_s3_bucket" "bucket" {
  bucket = "terraform-state-bnet"
  versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
  object_lock_configuration {
    object_lock_enabled = "Enabled"
  }
  tags = {
    Name = "S3 Remote Terraform State Store"
  }
}